# Blog

 Sistema de blog que armazena os posts e comentários dos usuários. As operações: select, create, insert, delete devem ser armazenadas em procedures e o sistema deve invocá-las para executar a ação. As atividades que devem ser desenvolvidas são as seguintes:

 Atividades
    
  - Cadastrar posts; 
  - Cadastrar comments;
  - Inserir posts;
  - Inserir comments;
  - Retornar todos os posts;
  - Retornar todos os comments;
  - Retornar todos os posts que tem comments;
  - Remover um comment;
  - Alterar um post

## Implementação

Instalação de extensões no VS Code, executando o comando `Ctrl + Shift + X` e selecionando as seguintes extensões:

  - spring boot extension pack (suporte para manipulação de arquivos .properties e .yml);
  - java extension pack;

Execução do comando `Ctrl + Shift + P`

Seleção:
    
  - Spring Initializr: Generate Maven Project
  - Spring Boot Dashboard (Auxilia na inicialização do projeto)

Instalação dos seguintes plugins:
    
  - Web (Tomcat e Spring MVC)
  - DevTools (Ferramentas de desenvolvimento do Spring Boot)
  - Actuator

Configuração do arquivo **application.properties**
    
  - `server.port=8080`  
  - `spring.datasource.url=jdbc:sqlserver://localhost;databaseName=blog`
  *É necessário configurar a porta padrão (1433) do Sql Server no Sql Server 2017 Configuration Manager para o driver efetuar a interface com o banco*
  - `spring.datasource.username=sa`
  - `spring.datasource.password=123`
  *Senha criada durante a instalação do Sql Server 2017 Management*
  - `spring.datasource.driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver`  
  - `spring.jpa.show-sql=true`
  *Para fins de debug*
  - `spring.jpa.hibernate.dialect=org.hibernate.dialect.SQLServer2017Dialect`

Configuração da classe de inicialização do spring boot

  - A anotação *@GetMapping(value="/home")* é usada para gerenciar as requisições GET do cliente
  - O valor *"/home"* é apenas usado para especificar o endpoint, ele pode ser alterado

# Links

Alguns links consultados para desenvolver ou auxiliar no desenvolvimento:

  - [Spring Boot](http://spring.io/projects/spring-boot/)
  - [Spring Boot + VS Code](https://code.visualstudio.com/docs/java/java-spring-boot/)
  - [Maven | Sql Server](https://mvnrepository.com/artifact/com.microsoft.sqlserver/mssql-jdbc/)
  - [SQLServer](https://www.microsoft.com/pt-br/sql-server/sql-server-downloads/)
