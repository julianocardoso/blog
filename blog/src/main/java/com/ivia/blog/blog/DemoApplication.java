package com.ivia.blog.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

	/***
	 * A anotação @GetMapping(value="/home") é usada para gerenciar as requisições GET do cliente
	 * O valor "/home" é apenas usado para especificar o endpoint, ele pode ser alterado
	 * @return Uma string que representa a resposta da requisição
	 */
	@GetMapping(value="/home")
	public String getHomePage(){
		return "Home page";
	}

	@GetMapping()
	public String getRootPage(){
		return "Root page";
	}
	
	public static void main(String[] args) {
		System.out.println("[ARGUMENTOS]: " + args); // Debug
		SpringApplication.run(DemoApplication.class, args);
		
	}

}

